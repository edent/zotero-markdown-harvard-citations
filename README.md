# Zotero MarkDown Harvard Citations

Allow in-text citations to have MarkDown links to their source - like `([Smith, 1984](https://doi.org/123.456))`

Follows Cite Them Right's Harvard Citation Style.

Includes code from https://github.com/bwiernik/zotero-tools/blob/master/apa-doi-in-text.csl

[This work is licensed under a Creative Commons Attribution-ShareAlike 4.0 License](http://creativecommons.org/licenses/by-sa/4.0/)
